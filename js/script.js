// Задание
// Реализовать функцию подсчета факториала числа.
//
//     Технические требования:
//
//     Считать с помощью модального окна браузера число, которое введет пользователь.
//     С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
//     Использовать синтаксис ES6 для работы с перемеными и функциями.
//
//
//     Не обязательное задание продвинутой сложности:
//
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе их указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

function factorialNum() {
    let number = prompt('Input the number for  factorial function.');
    while (isNaN(number) || +number <= 0) {
        number = prompt('Number is invalid! Input again the number for  factorial function.', number);
    }
    function factorial(number){
        if (+number === 1) {
            return number;
        }

        number *= factorial(number - 1);

        return number;
    }
    return alert(number + '!=' + factorial(number));
}

factorialNum();
